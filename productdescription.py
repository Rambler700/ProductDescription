# -*- coding: utf-8 -*-
"""
Created on Thu Jan 18 11:47:52 2018

@author: enovi
"""

import random

#Make product descriptions!

def main():
    value_1   = ''
    value_2   = ''
    text_1    = ''
    output_1  = ''
    text_2    = ''
    output_2  = ''
    value_3   = 0
    #Start with mattress qualities
    print('Enter the words that describe your mattress.')
    value_1 = input('Is the mattress hard or soft? ')
    value_2 = input('What material is the mattress made from? ')
    value_3 = int(input('How long is the mattress, in inches? '))
    #Generate descriptors
    text_1    = firstsentence()
    output_1  = hardness(value_1)
    text_2    = secondsentence()
    output_2  = value_2
    output_3  = thirdsentence(value_3)
    #Print out the text
    print('{} this {} mattress.'.format(text_1, output_1))
    print('It is {} {}.'.format(text_2, output_2))
    print('It is {}.'.format(output_3))
    
def firstsentence():
    descriptor = []
    selector = 0
    descriptor = ['Behold','Check out','Consider','Look at','Be amazed by','See']    
    selector = random.randint(0,len(descriptor) - 1)
    return descriptor[selector]
    
def hardness(value_1):
    descriptor = []
    selector = 0
    if value_1 == 'hard':
        descriptor = ['firm','sturdy','tough','solid']
    elif value_1 == 'soft':
        descriptor = ['comfortable','flexible','cozy','supple']
    else:
        #return empty value for now
        descriptor = ['____']
    selector = random.randint(0,len(descriptor) - 1)    
    return descriptor[selector]

def secondsentence():
    descriptor = []
    selector = 0
    descriptor = ['made from','genuine','carefully crafted','high-quality']    
    selector = random.randint(0,len(descriptor) - 1)
    return descriptor[selector]

def thirdsentence(value_3):
    descriptor = []
    selector = 0
    if value_3 >= 90:
        descriptor = ['spacious','roomy','kingly','lengthy']
    elif value_3 < 90 and value_3 > 70:
        descriptor = ['standard','normal','regular']
    elif value_3 <= 70:
        descriptor = ['compact','space-conserving','suitable for small spaces']
    else:
        #again, return empty line for now
        descriptor = ['____']        
    selector = random.randint(0,len(descriptor) - 1)
    return descriptor[selector]

main()